function onPalindromi(string) {
  if(string.length == 0 || string.length == 1) return true;
  if(string.charAt(0) != string.charAt(string.length - 1)) return false;
  
  return onPalindromi(string.substr(1, string.length - 2));
}
console.log(onPalindromi('markus'));
console.log(onPalindromi('imaami'));
console.log(onPalindromi('amaami'));