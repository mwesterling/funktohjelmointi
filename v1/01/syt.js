//Tehtävä 2, suurin yhteinen tekijä
var syt = function(p, q) {  
    if (q == 0) {  
        return p;  
    }  
  
    return syt(q, p % q);  
};  
console.log(syt(10, 0));
console.log(syt(50,2500));
console.log(syt(50,5000));

//Tehtävä 3, suhteelliset alkuluvut
function kjl(p,q) {
    if(syt(p,q) == 1) return true;
    return false;
}

console.log(kjl(35,18));
console.log(kjl(2,20));

//Tehtävä 4, potenssiin korotus
function potenssiin(kanta, eksponentti) {
  if (eksponentti == 0)
    return 1;
  else

    return kanta * potenssiin(kanta, eksponentti - 1);
}

console.log(potenssiin(5, 3));
console.log(potenssiin(10,10));

//Tehtävä 5, listan kääntö
function kaannaLista(kaannettava){
  var kaannetty = [];

  function kaanto(kaannettava){
    if (kaannettava.length !== 0){
      kaannetty.push(kaannettava.pop());
      kaanto(kaannettava );
    }
  }

  kaanto(kaannettava);
  return kaannetty;
}

console.log(kaannaLista([0,1,2,3,4,5,6,7,8,9]));