'use strict';

let f, g;
function foo() {
  let x;
  f = function() { return ++x; };
  g = function() { return --x; };
  x = 1;
  console.log('inside foo, call to f(): ' + f());
}
foo();  
console.log('call to g(): ' + g()); 
console.log('call to f(): ' + f()); 

/*On olemassa funktiot f ja g, joista f kasvattaa x:ää yhdellä ja g pienentää x:ää yhdellä
Koska x asetetaan ennen kutsuja arvoon 1, tulostukset menevät 1+1=2, 2-1=1 ja 1+1=2
Ilmeisesti rivin 11 funktiokutsu foo(); ajaa saman asian kuin funktioiden
kutsuminen suoraan foo() -funktion sisältä */